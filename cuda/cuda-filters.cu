#include "bmp_io.h"
#include "timer.h"
#include <iostream>
#include <math.h>
#include <stdlib.h>

using namespace std;

#define HISTOGRAM_SIZE 256
#define CONTRAST_THRESHOLD 80
#define FILTER_WIDTH 3
#define KCENTER_X FILTER_WIDTH / 2
#define KCENTER_Y FILTER_WIDTH / 2
#define FILTER_HEIGTH 3

float SOBEL_FILTER_X[FILTER_HEIGTH][FILTER_WIDTH] = {
        {-1, 0, 1},
        {-2, 0, 2},
        {-1, 0, 1}};
float SOBEL_FILTER_Y[FILTER_HEIGTH][FILTER_WIDTH] = {
        {1,  2,  1},
        {0,  0,  0},
        {-1, -2, -1}};

void save_histogram(unsigned int *histogram) {
    gray_image_t out =
            (gray_image_t) malloc(HISTOGRAM_SIZE * HISTOGRAM_SIZE * sizeof(float));

    unsigned max = 0;
    for (unsigned x = 0; x < HISTOGRAM_SIZE; x++) {
        if (histogram[x] > max)
            max = histogram[x];
    }

    for (unsigned x = 0; x < HISTOGRAM_SIZE; x++) {
        int val = HISTOGRAM_SIZE * histogram[x] / max;
        for (int y = 0; y < val; y++) {
            out[y * HISTOGRAM_SIZE + x] = 0xFF;
        }
        for (unsigned y = val; y < HISTOGRAM_SIZE; y++) {
            out[y * HISTOGRAM_SIZE + x] = 0;
        }
    }

    write_GrayBMP("histogram.bmp", out, HISTOGRAM_SIZE, HISTOGRAM_SIZE);
    free(out);
}

__global__ void RGBtoGray(color_image_t d_rgb, int imgS, gray_image_t d_gray) {
}

__global__ void histogram_1D(gray_image_t d_image, int imgS,
                             unsigned int *d_histogram) {
}

__global__ void contrast_1D(gray_image_t d_image, int imgS,
                            unsigned int *d_histogram, int min, int max,
                            int diff) {
}

__global__ void convolution2D(gray_image_t d_in, int imgW, int imgH,
                              float *d_filter, gray_image_t d_out) {
    const int idx = threadIdx.x + blockDim.x * blockIdx.x;
    const int idy = threadIdx.y + blockDim.y * blockIdx.y;
    // find center position of kernel (half of kernel size)
    const int m_start = idy < kCenterY ? kCenterY - idy : 0;
    const int n_start = idx < kCenterX ? kCenterX - xdx : 0;
    const int m_end = idy + FILTER_HEIGTH >= imgH ? imgH - idy - kCenterY : FILTER_HEIGTH;
    const int n_end = idx + FILTER_HEIGTH >= imgW ? imgW - idx - kCenterX : FILTER_WIDTH;

    for (int m = m_start; m < m_end; ++m) {
        for (int n = n_start; n < n_end; ++n) {
            int yy = idy + (m - KCENTER_Y);
            int xx = idx + (n - KCENTER_X);
            d_out[idy * imgW + idx] += d_in[yy * imgW + xx] * d_filter[m*FILTER_WIDTH + n];
        }
    }

}

__global__ void combineImages(gray_image_t d_in1, gray_image_t d_in2,
                              int imgS) {
}

int main(int argc, char *argv[]) {
    int imgW, imgH;
    color_image_t image;
    unsigned int histogram[HISTOGRAM_SIZE];

    if (argc < 2) {
        cerr << "Not enough arguments! Bailing out..." << endl;
        return -1;
    }

    if ((image = read_BMP(argv[1], &imgW, &imgH)) == NULL) {
        cerr << "Cannot read BMP ... ?! " << endl;
        return 1;
    }

    int imgSize = imgW * imgH;
    timer rgbToGrayTimer("rgb to gray");
    timer histogramTimer("histogram");
    timer contrastTimer("contrast");
    timer convolutionTimer("convolution");

    // Convert to grayscale image
    rgbToGrayTimer.start();
    gray_image_t gray = 0;
    // RGBtoGray
    rgbToGrayTimer.stop();

    free(image);
    write_GrayBMP("gray.bmp", gray, imgW, imgH);

    // Compute Histogram
    histogramTimer.start();
    // histogram_1D
    histogramTimer.stop();

    save_histogram(histogram);

    // Contrast Enhancement
    contrastTimer.start();
    // contrast_1D
    contrastTimer.stop();

    write_GrayBMP("contrast.bmp", gray, imgW, imgH);

    // Convolution
    convolutionTimer.start();
    gray_image_t outX = 0;
    gray_image_t outY = 0;
    // convolution2D
    // convolution2D
    // combineImages;
    convolutionTimer.stop();

    write_GrayBMP("convolution.bmp", outX, imgW, imgH);

    cout << rgbToGrayTimer;
    cout << histogramTimer;
    cout << contrastTimer;
    cout << convolutionTimer;

    free(gray);
    free(outX);
    free(outY);

    return 0;
}
