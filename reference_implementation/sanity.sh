#!/bin/bash
IMAGE=$(pwd)/images/20.bmp

# CPU reference
echo "Compiling and running CPU filters"
cd sequential && make && ./filters $IMAGE && cd ..

# CUDA
echo "Compiling and running CUDA filters"
cd  cuda && make
srun -C A4000 --gres=gpu:1 ./cuda-filters $IMAGE
cd ..

# Compare
echo "Comparing images"
COMPARE=$(pwd)/cuda/compare_images
echo "[Contrast]"
${COMPARE} sequential/contrast.bmp cuda/contrast.bmp
echo "[Convolution]"
${COMPARE} sequential/convolution.bmp cuda/convolution.bmp
echo "[Gray]"
${COMPARE} sequential/gray.bmp cuda/gray.bmp
echo "[Histogram]"
${COMPARE} sequential/histogram.bmp cuda/histogram.bmp
