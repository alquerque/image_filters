#!/bin/bash
set -e

IMAGES=$(ls -S $PWD/images/*.bmp)

OLD_CODE=$(realpath $PWD/reference_implementation/sequential/)
NEW_CODE=$(realpath $PWD/sequential/)

timeit() {
  cmd=${@:2}
  times=$1

  for k in $(seq 1 $times); do
    echo "Execution number " ${k} \|$cmd\|
    ${cmd}
  done
}
# CPU reference
echo "Compiling and running CPU filters"
compile() {
  path=$1
  cwd=$PWD
  cd ${path}
  make
  cd ${cwd}

}

run_single_img() {
  a_path=$1
  b_path=$2
  img=$3
  cwd=$PWD
  a_exec=$(realpath ${a_path}/filters)
  b_exec=$(realpath ${b_path}/filters)

  echo "************************"
  echo "Executing original code"
  cd ${a_path}
  timeit 3 ${a_exec} $img
  echo "************************"
  echo "Executing optimized code"
  cd ${b_path}
  timeit 3 ${b_exec} $img
  echo "************************"
}

compare() {
  # Compare
  echo "Comparing images"
  COMPARE=${NEW_CODE}/compare_images
  echo "[Contrast]"
  ${COMPARE} ${OLD_CODE}/contrast.bmp ${NEW_CODE}/contrast.bmp
  echo "[Convolution]"
  ${COMPARE} ${OLD_CODE}/convolution.bmp ${NEW_CODE}/convolution.bmp
  echo "[Gray]"
  ${COMPARE} ${OLD_CODE}/gray.bmp ${NEW_CODE}/gray.bmp
  echo "[Histogram]"
  ${COMPARE} ${OLD_CODE}/histogram.bmp ${NEW_CODE}/histogram.bmp
}

run() {
  old_code=$1
  new_code=$2
  cwd=$PWD
  cd ${path}

  for img in ${IMAGES}; do
    echo ------------------------
    echo Testing run with $img with filesize
    du -h $img
    echo ------------------------
    run_single_img ${old_code} ${new_code} ${img}
    compare
    echo ------------------------
  done

  cd $cwd

  echo "Congratulations all the images compared successfully"
}


echo "Compiling " ${OLD_CODE}
compile $OLD_CODE

echo "Compiling " ${NEW_CODE}
compile $NEW_CODE



run ${OLD_CODE} ${NEW_CODE}


exit 0
# CUDA
echo "Compiling and running CUDA filters"
cd cuda && make
srun -C A4000 --gres=gpu:1 ./cuda-filters $IMAGE
cd ..

# Compare
echo "Comparing images"
COMPARE=$(pwd)/cuda/compare_images
echo "[Contrast]"
${COMPARE} sequential/contrast.bmp cuda/contrast.bmp
echo "[Convolution]"
${COMPARE} sequential/convolution.bmp cuda/convolution.bmp
echo "[Gray]"
${COMPARE} sequential/gray.bmp cuda/gray.bmp
echo "[Histogram]"
${COMPARE} sequential/histogram.bmp cuda/histogram.bmp
